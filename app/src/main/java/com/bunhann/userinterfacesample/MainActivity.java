package com.bunhann.userinterfacesample;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private Button btnActivity,  btnTextView, btnWebActivity;

    private ImageButton btnHeart;

    private ImageView imgHomeInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        btnHeart = findViewById(R.id.btnHeart);
        imgHomeInfo = findViewById(R.id.imgHomeInfo);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //Intent intent = new Intent(view.getContext(), TestActivity.class);
                //Intent intent = new Intent(view.getContext(), TextViewActivity.class);
                //Intent intent = new Intent(view.getContext(), ToggleActivity.class);
                Intent intent = new Intent(view.getContext(), SpinnerActivity.class);
                //Intent intent = new Intent(view.getContext(), AutoCompleteTextViewActivity.class);
                //Intent intent = new Intent(view.getContext(), ProgressBarActivity.class);
                //Intent intent = new Intent(view.getContext(), DialogActivity.class);
                startActivity(intent);
            }
        });

        btnActivity = (Button) findViewById(R.id.btnActivity);
        btnActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ButtonActivity.class);
                startActivity(intent);
            }
        });



        btnTextView = (Button) findViewById(R.id.btnTextView);
        btnTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EditTextActivity.class);
                startActivity(intent);
            }
        });


        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KhmerOSmuollight.ttf");
        btnActivity.setTypeface(font);

        btnHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CheckboxActivity.class);
                startActivity(intent);
            }
        });

        imgHomeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), NumberToKhmerDigitActivity.class);
                startActivity(intent);
            }
        });

        btnWebActivity = findViewById(R.id.btnWebActivity);
        btnWebActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Webview_Activity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
