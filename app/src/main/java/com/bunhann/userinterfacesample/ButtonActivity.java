package com.bunhann.userinterfacesample;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Objects;

public class ButtonActivity extends AppCompatActivity {

    private Button button;
    private Button button2, btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.button_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Button Text Only", Toast.LENGTH_SHORT).show();
            }
        });

        btn3 = findViewById(R.id.button3);
        btn3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(v.getContext(), "Long Click!", Toast.LENGTH_SHORT).show();
                Log.d("BUTTON", "LongClick Action");
                return false;
            }
        });


    }

    private void button2Click(View v){
        Toast.makeText(v.getContext(), "Button 2 is click!", Toast.LENGTH_SHORT).show();
    }
}
