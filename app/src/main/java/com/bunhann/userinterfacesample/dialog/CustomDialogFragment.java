package com.bunhann.userinterfacesample.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.bunhann.userinterfacesample.R;

public class CustomDialogFragment extends DialogFragment {

    private EditText edName, edPassword;
    private Button btnOk, btnCancel;

    public CustomDialogFragment() {

        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below

    }


    public static CustomDialogFragment newInstance(String title) {

        Bundle args = new Bundle();

        CustomDialogFragment fragment = new CustomDialogFragment();

        args.putString("DIALOG_TITLE", title);
        fragment.setArguments(args);

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_custom, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edName = (EditText) view.findViewById(R.id.edDiaName);
        edPassword = (EditText) view.findViewById(R.id.edPassword);

        btnOk = (Button) view.findViewById(R.id.btnSubmitDialog);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        this.setCancelable(false);

        String title = getArguments().getString("DIALOG_TITLE", "User Info");
        getDialog().setTitle(title);

        // Show soft keyboard automatically and request focus to field
        edName.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
