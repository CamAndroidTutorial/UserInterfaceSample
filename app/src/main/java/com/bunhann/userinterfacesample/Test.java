package com.bunhann.userinterfacesample;

public class Test {

    public static void main(String[] args){

        System.out.println(numberToKhmer("7893014"));

    }
    private static String numberToKhmer(String number){
        String result="";

        String[] digit = {"0","1","2", "3","4", "5", "6" ,"7", "8", "9"};
        String[] khmerDigit = {"០","១","២", "៣","៤", "៥", "៦", "៧", "៨","៩"};
       // System.out.println(String.valueOf(number.length()));
        for (int i =0;i<number.length(); i++){
            //System.out.println(String.valueOf(number.substring(i)));
            for (int j=0;j<digit.length;j++){
                if (String.valueOf(number.charAt(i)).equalsIgnoreCase(digit[j])){
                    result = result.concat(khmerDigit[j]);
                    break;
                }
            }
        }
        return result;
    }

}
