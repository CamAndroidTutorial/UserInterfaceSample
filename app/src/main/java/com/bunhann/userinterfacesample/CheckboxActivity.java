package com.bunhann.userinterfacesample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CheckboxActivity extends AppCompatActivity {

    private CheckBox chk1, chk2, chk3;
    private Button btnSubmit;

    private TextView tvGenderSelect;
    private RadioButton rdo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.checkbox_activity);

        btnSubmit = findViewById(R.id.btnSubmitCheckbox);

        chk1 = findViewById(R.id.checkbox1);
        chk2 = findViewById(R.id.checkbox2);
        chk3 = findViewById(R.id.checkbox3);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk1.isChecked()){
                    Toast.makeText(CheckboxActivity.this, "Checked 1", Toast.LENGTH_SHORT).show();
                }
                if (chk2.isChecked())
                    Toast.makeText(CheckboxActivity.this, "Checked 2", Toast.LENGTH_SHORT).show();
            }
        });

        tvGenderSelect = findViewById(R.id.tvGenderSelect);
        tvGenderSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup genderOption = (RadioGroup) findViewById(R.id.genderOption);
                genderOption.clearCheck();

            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Check to see if a button has been clicked.
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked.
        switch(view.getId()) {
            case R.id.rdMale:
                if (checked)
                    tvGenderSelect.setText(getString(R.string.tvGender) + " Male");
                    break;
            case R.id.rdFemale:
                if (checked)
                    tvGenderSelect.setText(getString(R.string.tvGender) + " Female");
                    break;
            case R.id.rdNA:
                if (checked)
                    tvGenderSelect.setText(getString(R.string.tvGender) + " N/A");
                    break;
        }
    }
}
