package com.bunhann.userinterfacesample;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class ProgressBarActivity extends AppCompatActivity {

    int progress = 0;

    ProgressBar progressBarCircle, progressBar2, progressBar3;

    private Button btnProgressState, btnProgressDialog1, btnProgressDialog2;

    //For Progress Dialog
    private ProgressDialog progressDialog;
    Handler handler = new Handler();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.progressbar_layout);

        progressBarCircle = findViewById(R.id.progressBarCircle);

        progressBar2 = findViewById(R.id.progressBar2);

        progressBar3 = findViewById(R.id.progressBar3);

        btnProgressState = findViewById(R.id.btnProgressState);

        btnProgressState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // set the progress
                progressBarCircle.setProgress(progress);
                progressBar2.setProgress(progress);
                progressBar3.setProgress(progress);
                btnProgressState.setText("STOP");
                btnProgressState.setEnabled(false);
                // thread is used to change the progress value
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (progress < progressBar3.getMax()){
                            try {
                                Thread.sleep(1000);

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            progressBarCircle.setProgress(progress + 10);
                            progressBar2.setProgress(progress + 10);
                            progressBar3.setProgress(progress + 10);
                            progress += 10;

                            if (progressBar3.getProgress()>=progressBar3.getMax()){

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnProgressState.setText("START");
                                        btnProgressState.setEnabled(true);
                                        progress = 0;
                                        progressBar3.setProgress(progress);
                                        progressBarCircle.setProgress(progress);
                                        progressBar2.setProgress(progress);

                                        progressBarCircle.setIndeterminate(false);
                                        progressBar2.setIndeterminate(false);
                                    }
                                });
                            }


                        }

                    }
                });
                thread.start();
            }
        });

        btnProgressDialog1 = findViewById(R.id.btnProgressDialog1);
        btnProgressDialog1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(v.getContext());
                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle("Progress Dialog 1"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(10000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }).start();
            }
        });

        btnProgressDialog2 = findViewById(R.id.btnProgressDialog2);


        btnProgressDialog2.setOnClickListener(new View.OnClickListener() {
            Handler handle = new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    progressDialog.incrementProgressBy(2); // Incremented By Value 2
                }
            };

            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(v.getContext());
                progressDialog.setMax(100); // Progress Dialog Max Value
                progressDialog.setMessage("Downloading..."); // Setting Message
                progressDialog.setTitle("Progress Dialog 2"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL); // Progress Dialog Style Horizontal
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (progressDialog.getProgress() <= progressDialog.getMax()) {
                                Thread.sleep(200);
                                handle.sendMessage(handle.obtainMessage());
                                if (progressDialog.getProgress() == progressDialog.getMax()) {
                                    progressDialog.dismiss();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

    }
}
