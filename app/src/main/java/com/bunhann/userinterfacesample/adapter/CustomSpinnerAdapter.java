package com.bunhann.userinterfacesample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bunhann.userinterfacesample.R;

public class CustomSpinnerAdapter extends BaseAdapter {

    Context context;
    int[] img;
    String[] strName;
    LayoutInflater inflater;


    public CustomSpinnerAdapter(Context context, int[] img, String[] strName) {
        this.context = context;
        this.img = img;
        this.strName = strName;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return img.length;
    }

    @Override
    public Object getItem(int position) {
        return strName[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.custom_spinner, null);

        ImageView icon = convertView.findViewById(R.id.imgCustomSpinner);
        TextView names = convertView.findViewById(R.id.tvCustomSpinner);
        icon.setImageResource(img[position]);
        names.setText(strName[position]);

        return convertView;
    }
}
