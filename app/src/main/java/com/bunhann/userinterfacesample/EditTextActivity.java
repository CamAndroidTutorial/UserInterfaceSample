package com.bunhann.userinterfacesample;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Objects;

public class EditTextActivity extends AppCompatActivity {

    private TextView txtName, txtPassword;

    private EditText edName;

    private EditText edEmail, edPassword, edMutipleline;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.textview_edittext_layout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        //Initial Object with our View
        txtName = (TextView) findViewById(R.id.txtName);

        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.input_email);

        edPassword = findViewById(R.id.edPassword);

        //edPassword.setInputType(InputType.TYPE_CLASS_NUMBER);

        //TextInputLayout inputLayout = findViewById(R.id.input_layout_email);


        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.d("TXTWATCHER", "Before Change");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("TXTWATCHER", "After Text Change");
            }
        });

        setupFloatingLabelError();

    }


    private void setupFloatingLabelError() {
        final TextInputLayout floatingEmailLabel = (TextInputLayout) findViewById(R.id.input_layout_email);
        floatingEmailLabel.getEditText().addTextChangedListener(new TextWatcher() {
            // ...
            @Override
            public void onTextChanged(CharSequence text, int start, int count, int after) {
                if (text.length() > 0 && text.length() <= 4) {
                    floatingEmailLabel.setError(getString(R.string.email_required));
                    floatingEmailLabel.setErrorEnabled(true);
                } else {
                    floatingEmailLabel.setErrorEnabled(false);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
