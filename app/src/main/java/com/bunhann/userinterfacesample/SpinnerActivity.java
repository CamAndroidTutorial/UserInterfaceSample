package com.bunhann.userinterfacesample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.bunhann.userinterfacesample.adapter.CustomSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SpinnerActivity extends AppCompatActivity {

    private Spinner spinner2, spinner1, customSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addItemsOnSpinner2();

        addCustomSpinner();

    }

    public void addItemsOnSpinner2() {

        // Create the spinner.
        spinner2 = (Spinner) findViewById(R.id.spinner2);

        final List<String> list = new ArrayList<String>();
        list.add("Yahoo");
        list.add("Microsoft");
        list.add("Google");
        list.add("Facebook");
        list.add("Github");
        list.add("Apple");
        list.add("Bing");
        list.add("Amazon");
        list.add("Netflix");
        list.add("Youtube");

        // Create ArrayAdapter using the string array and default spinner layout.
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);

        // Specify the layout to use when the list of choices appears.
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner.
        spinner2.setAdapter(dataAdapter);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), list.get(position).toUpperCase(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void addCustomSpinner(){

        final String[] companyName = {"Facebook", "Google", "Twitter"};
        int[] logo = {R.drawable.facebook, R.drawable.google, R.drawable.twitter};


        customSpinner = (Spinner) findViewById(R.id.spinnerCustom);

        CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(this, logo, companyName);
        customSpinner.setAdapter(customSpinnerAdapter);

        customSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(SpinnerActivity.this, companyName[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
