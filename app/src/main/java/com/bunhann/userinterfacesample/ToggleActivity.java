package com.bunhann.userinterfacesample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class ToggleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toggle_switch_layout);

        Switch aSwitch = (Switch) findViewById(R.id.switch1);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

    }

    public void onToggleClick(View view) {
        ToggleButton toggle = (ToggleButton) findViewById(R.id.my_toggle);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              StringBuffer onOff = new StringBuffer().append("It is on!");
              if (isChecked) { // The toggle is enabled
                  Toast.makeText(getApplicationContext(), onOff.toString(),
                          Toast.LENGTH_SHORT).show();
              }
          }
      });
    }

    public void onSwitchClick(View view) {
        Switch aSwitch = (Switch) findViewById(R.id.switch1);
        aSwitch.setOnCheckedChangeListener(new
           CompoundButton.OnCheckedChangeListener() {
               public void onCheckedChanged(CompoundButton buttonView,
                                            boolean isChecked) {
                   StringBuffer onOff = new StringBuffer().append("On or off? ");
                   if (isChecked) { // The switch is enabled
                       onOff.append("ON ");
                   } else { // The switch is disabled
                       onOff.append("OFF ");
                   }
                   Toast.makeText(getApplicationContext(), onOff.toString(),
                           Toast.LENGTH_SHORT).show();
               }
           });
    }
}
