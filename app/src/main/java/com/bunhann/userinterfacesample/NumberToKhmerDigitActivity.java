package com.bunhann.userinterfacesample;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.bunhann.userinterfacesample.utils.NumberToWordsConverter;

import java.util.Objects;

public class NumberToKhmerDigitActivity extends AppCompatActivity {

    private TextInputEditText edEngNumber;

    private TextView tvDigitLabel, tvKhmerDigit, tvEngWordNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.number_khmer_digit);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(getSupportActionBar()).setHomeButtonEnabled(true);
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }

        edEngNumber = findViewById(R.id.edEngNumber);
        tvDigitLabel = findViewById(R.id.tvDigitLabel);
        tvKhmerDigit = findViewById(R.id.tvKhmerDigit);
        tvEngWordNumber = findViewById(R.id.tvEngWordNumber);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KhmerOSmuollight.ttf");
        tvKhmerDigit.setTypeface(font);

        edEngNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                tvDigitLabel.setVisibility(View.INVISIBLE);
                tvKhmerDigit.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count>10){

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                tvKhmerDigit.setText(numberToKhmer(edEngNumber.getText().toString()));
                tvDigitLabel.setVisibility(View.VISIBLE);
                tvKhmerDigit.setVisibility(View.VISIBLE);
                if (edEngNumber.getText().length()>0){
                    tvEngWordNumber.setText("In word: " + NumberToWordsConverter.convert(Integer.parseInt(edEngNumber.getText().toString())));
                } else tvEngWordNumber.setText("");
            }
        });
    }

    private String numberToKhmer(String number){
        String result="";

        char[] digit = {'0','1','2', '3','4', '5', '6' ,'7', '8', '9'};
        String[] khmerDigit = {"០","១","២", "៣","៤", "៥", "៦", "៧", "៨","៩"};
        //char[] khmerDigit = {"០","១","២", "៣","៤", "៥", "៦", "៧", "៨","៩"};
        for (int i =0;i<number.length(); i++){
            for (int j=0;j<digit.length;j++){
                if (number.charAt(i)==(digit[j])){
                    result = result.concat(khmerDigit[j]);
                    break;
                }
            }
        }
        return result;
    }

}
