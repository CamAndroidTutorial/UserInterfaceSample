package com.bunhann.userinterfacesample;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

public class TextViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.textview_activity);

        TextView view = (TextView)findViewById(R.id.tvHtml);
        TextView tvHtml2 = (TextView)findViewById(R.id.tvHtml2);

        String formattedText = "This <i>is</i> a <b>test</b> of <a href='http://foo.com'>html.</a> ";
        // or getString(R.string.htmlFormattedText);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            view.setText(Html.fromHtml(formattedText, Html.FROM_HTML_MODE_LEGACY));
            tvHtml2.setText(Html.fromHtml(getString(R.string.htmlFormattedText), Html.FROM_HTML_MODE_LEGACY));
        } else {

            view.setText(Html.fromHtml(formattedText));
            view.append(Html.fromHtml("Nice! <font color='#c5c5c5'>This text has a color</font>. This doesn't"));
            tvHtml2.setText(Html.fromHtml(getString(R.string.htmlFormattedText)));
        }


        // Get access to our TextView
        TextView txtCustomFont = (TextView) findViewById(R.id.tvCustomFont);
        // Create the TypeFace from the TTF asset
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/KhmerOSmuollight.ttf");
        // Assign the typeface to the view
        txtCustomFont.setTypeface(font);
        txtCustomFont.setText(getString(R.string.title_app));

    }
}
