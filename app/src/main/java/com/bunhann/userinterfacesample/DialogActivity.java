package com.bunhann.userinterfacesample;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bunhann.userinterfacesample.dialog.CustomAlertDialogFragment;
import com.bunhann.userinterfacesample.dialog.CustomDialogFragment;
import com.bunhann.userinterfacesample.dialog.DatePickerFragment;
import com.bunhann.userinterfacesample.dialog.TimePickerFragment;

public class DialogActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAlert, btnAlert2, btnAlert3;
    private Button btnDateDialog, btnTimeDialog;

    private Button btnCustomDialog, btnCustomDialogFragment, btnCustomAlert;

    private TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        btnAlert = findViewById(R.id.btnAlert);
        btnAlert2 = findViewById(R.id.btnAlert2);
        btnAlert3 = findViewById(R.id.btnAlert3);

        btnDateDialog = findViewById(R.id.btnDateDialog);
        btnTimeDialog = findViewById(R.id.btnTimeDialog);

        tvDate = findViewById(R.id.tvDate);

        btnAlert.setOnClickListener(this);
        btnAlert2.setOnClickListener(this);
        btnAlert3.setOnClickListener(this);
        btnDateDialog.setOnClickListener(this);
        btnTimeDialog.setOnClickListener(this);

        btnCustomDialog = findViewById(R.id.btnCustomDialog);
        btnCustomDialog.setOnClickListener(this);

        btnCustomDialogFragment = findViewById(R.id.btnCustomDialogFragment);
        btnCustomDialogFragment.setOnClickListener(this);

        btnCustomAlert = findViewById(R.id.btnCustomAlert);
        btnCustomAlert.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        final AlertDialog alertDialog = new AlertDialog.Builder(v.getContext()).create();

        final FragmentManager fm = getSupportFragmentManager();

        int id = v.getId();
        switch (id){
            case R.id.btnAlert:
                // Setting Dialog Title
                alertDialog.setTitle("Alert Dialog");
                // Setting Dialog Message
                alertDialog.setMessage("Welcome to Android Course");
                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_error);
                // Setting OK Button
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog closed
                        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                    }
                });
                // Showing Alert Message
                alertDialog.show();
                break;

            case R.id.btnAlert2:
                // Setting Dialog Title
                alertDialog.setTitle("Alert Dialog");
                // Setting Dialog Message
                alertDialog.setMessage("Welcome to Android Course");
                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.ic_error);
                // Setting OK Button
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog closed
                        Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                    }
                });
                // Setting NO Button
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(alertDialog.getContext(), "You Click No!", Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.setCancelable(false);

                // Showing Alert Message
                alertDialog.show();
                break;
            case R.id.btnAlert3:
                // Home Work

                break;

            case R.id.btnDateDialog:
                DialogFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getSupportFragmentManager(), "DATE");
                break;

            case R.id.btnTimeDialog:
                DialogFragment timePickerFragment = new TimePickerFragment();
                timePickerFragment.show(getSupportFragmentManager(), "TIME");
                break;

            case R.id.btnCustomDialog:
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.dialog_custom);
                final EditText editText = dialog.findViewById(R.id.edDiaName);
                Button btnOk = dialog.findViewById(R.id.btnSubmitDialog);
                Button btnCancel = dialog.findViewById(R.id.btnCancel);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), editText.getText().toString(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.btnCustomDialogFragment:

                CustomDialogFragment customDialogFragment = CustomDialogFragment.newInstance("User Information");
                customDialogFragment.show(fm, "CUSTOM_DIALOG_FRAGMENT");

                break;

            case R.id.btnCustomAlert:
                CustomAlertDialogFragment customAlertDialogFragment = CustomAlertDialogFragment.newInstance("Confirm Delete", "Are you sure?");
                customAlertDialogFragment.show(fm, "CUSTOM_AlERT_DIALOG_FRAGMENT");
                break;
        }
    }

    public void processDatePickerResult(int year, int month, int day) {
        // The month integer returned by the date picker starts counting at 0
        // for January, so you need to add 1 to show months starting at 1.
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        // Assign the concatenated strings to dateMessage.
        String dateMessage = (month_string + "/" + day_string + "/" + year_string);
        Toast.makeText(this, "Date: " + dateMessage, Toast.LENGTH_SHORT).show();
        tvDate.setText("DATE: " + dateMessage);
    }

    public void processTimePickerResult(int hourOfDay, int minute) {
        // Convert time elements into strings.
        String hour_string = Integer.toString(hourOfDay);
        String minute_string = Integer.toString(minute);
        // Assign the concatenated strings to timeMessage.
        String timeMessage = (hour_string + ":" + minute_string);
        Toast.makeText(this, "Time: " + timeMessage, Toast.LENGTH_SHORT).show();
    }
}
