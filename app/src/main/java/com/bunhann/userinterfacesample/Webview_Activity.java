package com.bunhann.userinterfacesample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class Webview_Activity extends AppCompatActivity implements View.OnClickListener{

    WebView simpleWebView;
    Button loadWebPage, loadFromStaticHtml;

    public final static String url = "https://google.com";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_layout);

        simpleWebView = findViewById(R.id.webview);

        loadFromStaticHtml = findViewById(R.id.btnStaticHtml);
        loadWebPage = findViewById(R.id.btnDynamicHtml);


        simpleWebView.setWebViewClient(new MyWebViewClient());
        WebSettings webSettings =simpleWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowContentAccess(true);
        webSettings.setBuiltInZoomControls(true);

        loadFromStaticHtml.setOnClickListener(this);
        loadWebPage.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnDynamicHtml:
                simpleWebView.loadUrl(url); // load a web page in a web view
                break;
            case R.id.btnStaticHtml:
                simpleWebView.loadUrl("file:///android_asset/luxe/index.html");
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.simpleWebView.canGoBack()) {
            this.simpleWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String url) {
            //if internal link, open in webview
            webView.loadUrl(url);
            return false;
        }
    }
}
